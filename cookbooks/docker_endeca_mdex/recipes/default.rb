# Cookbook Name:: docker_endeca_mdex
# Recipe:: default
#
# Copyright (c) 2016 Techbiosis, All Rights Reserved.
#


# Pull latest image
docker_image 'centos' do
  tag '6'
    action :pull
end

# Run container exposing ports
docker_container 'centos' do
  repo 'centos'
  tag '6'
  port '80:80'
  host_name 'www'
  domain_name 'computers.biz'
end

directory "#{Chef::Config[:file_cache_path]}" do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

# Download docker_endeca_mdex
remote_file "#{node['chef_client']['file_cache_path']}/#{node['docker_endeca_mdex']['mdex_install_archive_name']}" do
    source "#{node['download_url']}#{node['docker_endeca_mdex']['remote_uri']}#{node['docker_endeca_mdex']['mdex_install_archive_name']}"
    retries 3
    checksum "#{node['endeca_mdex']['checksum']}"
    not_if { File.exists?("#{node['chef_client']['file_cache_path']}/#{node['docker_endeca_mdex']['mdex_install_archive_name']}") }
end

unless Dir.exists?("#{node['docker_endeca_mdex']['endeca_mdex_home']}")
    # Extract docker_endeca_mdex compressed file
    execute "extract_docker_endeca_mdex_install_archive" do
        cwd Chef::Config[:file_cache_path]
        command "unzip -oq #{node['chef_client']['file_cache_path']}/#{node['docker_endeca_mdex']['mdex_install_archive_name']} -d #{Chef::Config[:file_cache_path]}/mdex_installer"
        creates "#{Chef::Config[:file_cache_path]}/mdex_installer/#{node['docker_endeca_mdex']['mdex_install_file_name']}"
    end

    # Change docker_endeca_mdex install file permission
    file "#{Chef::Config[:file_cache_path]}/#{node['docker_endeca_mdex']['mdex_install_file_name']}" do
        mode '0744'
    end

    # The silent file is not fully generic
    template "#{Chef::Config[:file_cache_path]}/mdex_installer/mdex_silent_response.rsp.properties" do
        source 'mdex_silent_response.rsp.properties.erb'
        mode '0644'
        variables(
            :mdexRoot => "#{node['docker_endeca_mdex']['endeca_mdex_home']}/#{node['endeca']['mdex_version']}"
        )
        sensitive true
    end

    # Change endeca mdex folder permission
    directory "#{node['docker_endeca_mdex']['endeca_mdex_home']}" do
        owner node['user']
        group node['user_group']
        mode '0755'
        recursive true
    end

    # Install Endeca MDEX Silent Mode
    execute "install_docker_endeca_mdex_silent_mode" do
        cwd "#{Chef::Config[:file_cache_path]}/mdex_installer"
        user node['user']
        group node['user_group']
        command "./#{node['docker_endeca_mdex']['mdex_install_file_name']} -f 'mdex_silent_response.rsp.properties' -i silent"
        timeout 900
    end

    # Copy mdex_setup_sh.ini from template to /opt/endeca/MDEX/6.5.2
    template "#{Chef::Config[:file_cache_path]}/mdex_installer/mdex_setup_sh.ini" do
        source 'mdex_setup_sh.ini.erb'
        mode '0644'
        variables(
            :mdexRoot => "#{node['docker_endeca_mdex']['endeca_mdex_home']}/#{node['endeca']['mdex_version']}"
        )
        sensitive true
    end

    # Add ENDECA_MDEX_ROOT environment variable
    template "/etc/profile.d/mdex.sh" do
        source 'mdex_setup_sh.ini.erb'
        mode '0644'
        variables(
            :mdexRoot => "#{node['docker_endeca_mdex']['endeca_mdex_home']}/#{node['endeca']['mdex_version']}"
        )
        sensitive true
    end

    # Fix up /opt/endeca/MDEX ownership
    execute "ownership /opt/endeca/MDEX" do
        command "chown -R #{node['user']}:#{node['user_group']} #{node['docker_endeca_mdex']['endeca_mdex_home']}"
        timeout 900
    end

    execute "permissions /opt/endeca/MDEX" do
        command "chmod -R 755 #{node['docker_endeca_mdex']['endeca_mdex_home']}"
        timeout 900
    end

    execute "remove uninstall symlink for mdex" do
        command "unlink /home/#{node['user']}/Uninstall_MDEX_#{node['endeca']['mdex_version']} | true"
    end

    #Printing Endeca TF installed message
    bash "Endeca MDEX installed" do
       code <<-EOF
       echo ' '
       echo '############ Endeca MDEX installed by chef! ############'
       echo ' '
    EOF
    end
    end

