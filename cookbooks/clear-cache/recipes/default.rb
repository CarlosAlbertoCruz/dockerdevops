sleep 10

execute "clear_cache" do
    command "find #{Chef::Config[:file_cache_path]}/* ! -name 'chef-client-running.pid' ! -name 'cookbooks' -prune -exec rm -rf {} +"
end
