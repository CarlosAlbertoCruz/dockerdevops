# Create /opt directory
directory "/opt" do
    action :create
    owner node['user']
    group node['user_group']
    mode '0775'
end

# Create /opt/endeca directory
directory "/opt/endeca" do
    action :create
    owner node['user']
    group node['user_group']
    mode '0775'
end

# Add hostname to hosts file
ruby_block "Adding host \"#{node['hostname']}\" to hosts file" do
  block do
    file = Chef::Util::FileEdit.new("/etc/hosts")
    file.search_file_replace(/127.0.0.1   localhost/,"127.0.0.1   #{node['hostname']} localhost")
    file.write_file
  end
  not_if { File.readlines("/etc/hosts").grep(/#{node['hostname']}/).size > 0 }
end

# Enable automatic login for user "vagrant"
template "/etc/gdm/custom.conf" do
    source 'custom.conf'
    mode '0644'
    sensitive true
    variables( :user => node['user'] )
    not_if { File.readlines("/etc/gdm/custom.conf").grep(/#{node['user']}/).size > 0 }
end

# Download OJDBC driver
remote_file "#{node['chef_client']['file_cache_path']}/#{node['ojdbc']['install_archive_name']}" do
    source "#{node['download_url']}#{node['ojdbc']['remote_uri']}#{node['ojdbc']['install_archive_name']}"
    retries 3
    checksum "#{node['ojdbc']['checksum']}"
    not_if { File.exists?("#{node['chef_client']['file_cache_path']}/#{node['ojdbc']['install_archive_name']}") }
end

# Disable screensaver
execute "Disable screensaver" do
    command "su - #{node['user']} bash -c \"gconftool-2 --set --type=bool /apps/gnome-screensaver/idle_activation_enabled False\""
    action :run
end

# Disable background
execute "Disable background" do
    command "su - #{node['user']} bash -c \"gconftool-2 --set --type=bool /desktop/gnome/background/draw_background False\""
    action :run
end

# Create swap file
template "#{Chef::Config[:file_cache_path]}/create_swap_file.sh" do
  source 'create_swap_file.sh.erb'
  mode '0755'
  sensitive true
end

execute "create_swap_file" do
  cwd Chef::Config[:file_cache_path]
  command "./create_swap_file.sh"
  timeout 900
end

# Add aliases for log files
template "/etc/profile.d/alias.sh" do
  source 'alias.sh.erb'
  mode '0755'
  sensitive true
  variables(
    :aliasList => node['aliasList']
  )
end

#Various YUM packages
if ( !File.exist?("/usr/bin/git") )
    package 'git'
end

if ( !File.exist?("/usr/bin/unzip") )
    package 'unzip'
end
