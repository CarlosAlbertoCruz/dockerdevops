# Needs to include license

# General settings
default['chef_client']['file_cache_path']  = '/software'

default['hostname'] = "techbiosisvm"

default['host_download_folder'] = "/software"
default['guest_download_folder'] = "/var/chef/cache"

default['install_dir'] = "/opt"

default['user'] = "vagrant"
default['user_group'] = "vagrant"

default['projectName'] = "OracleCommerce"

# Alias list for log files and others
default['aliasList'] = {     # 'alias' => 'full-command'
    'g'  => '\'gradle\'',
    'll' => '\'ls -lart\'',
    'p'  => '\'cd /workspace/' + node['projectName'] + '\'',
    'lf' => '\'tail -2000f /data/logs/atg_production.out | /opt/ATG/ATGLogColorizer\'',
    'lm' => '\'tail -2000f /data/logs/atg_publishing.out | /opt/ATG/ATGLogColorizer\'',
    'lc' => '\'tail -2000f /data/logs/atg_csc.out | /opt/ATG/ATGLogColorizer\''
}

# Database dumps
default['dumps']['file_name'] = "neo_dump_20160830.zip"
default['dumps']['remote_uri'] = "/oracle/"

# Database schemas     'username' => 'password'
default['schemaList'] = {
    'live' => 'live',
    'cata' => 'cata',
    'catb' => 'catb',
    'ca' => 'ca',
    'stage' => 'stage',
    'svcagent' => 'svcagent'
}

# Software Download URL
default['download_url'] = "http://s3-us-west-2.amazonaws.com/biosisvm"

# Java Development Kit (fmw_jdk cookbook)
default['fmw_jdk']['remote_uri'] = "/java/"
default['fmw_jdk']['install_archive_name'] = "jdk-8u102-linux-x64.tar.gz"
default['fmw_jdk']['checksum'] = "7cfbe0bc0391a4abe60b3e9eb2a541d2315b99b9cb3a24980e618a89229e04b7"
node.override['fmw']['java_home_dir'] = "/usr/java/jdk1.8.0_102"
node.override['fmw_jdk']['source_file'] = node['host_download_folder'] + "/" + node['fmw_jdk']['install_archive_name']

# Oracle Commerce Platform/ATG (atg cookbook)
default['atg']['remote_uri'] = "/atg/11.2/"
default['atg']['install_file_name'] = "OCPlatform11_2.bin"
default['atg']['install_archive_name'] = "V78217-01-Oracle-Commerce-Platform-11.2-Linux.zip"
default['atg']['checksum'] = "e12aff823c117f686e71dba59c087b8ebd17bee059ab2fb6addb50d7cc4b3ab1"
default['atg']['atg_home'] = "/opt/ATG/ATG11.2"
default['atg']['atg_base'] = "/opt/ATG"
default['atg']['patch1_dir_name'] = "OCPlatform11.2_p1"
default['atg']['patch1_install_archive_name'] = "p23147552-Oracle-Commerce-Platform-patch-11.2.0.1.zip"
default['atg']['patch1_checksum'] = "6d974f511342a8d072a1d32c6b7539d37397eacd61f01ce8eaa8f1a113c8f6c1"

# Oracle Commerce Platform/ATG Customer Service Center (atg-csc cookbook)
default['atg-csc']['csc_home'] = node['atg']['atg_home'] + "/CSC11.2"
default['atg-csc']['install_file_name'] = "OCServiceCenter11.2_224RCN.bin"
default['atg-csc']['install_archive_name'] = "V78220-01-Oracle-Commerce-Service-Center-11.2-Linux.zip"
default['atg-csc']['checksum'] = "16111105bfbaf51ffdace5e76367bd241baebf296c3c1c2e14be541e3b86e886"
default['atg-csc']['patch1_install_archive_name'] = "p23147539-Oracle-Commerce-Service-Center-patch-11.2.0.1.zip"
default['atg-csc']['patch1_checksum'] = "7b666a58370bd72135035368b10e921b6dc5b7edd0127267e01ffd08c4a45e89"
default['atg-csc']['patch1_dir_name'] = "Service11.2_p1"

# Oracle Database (oracle11gR2 cookbook)
default['oradb11gR2']['remote_uri'] = "/oracle/"
default['oradb11gR2']['oradb11gR2_archive_name01'] = "linux.x64_11gR2_database_1of2.zip"
default['oradb11gR2']['oradb11gR2_archive_name01_checksum'] = "ff46188aea66967ffbe1e46a52ac85670a10bc8acea78536540c3b00c6d3b5dc"
default['oradb11gR2']['oradb11gR2_archive_name02'] = "linux.x64_11gR2_database_2of2.zip"
default['oradb11gR2']['oradb11gR2_archive_name02_checksum'] = "008dd2f9d45075cea9cc745fed8a2ce9e42d53afc91bb8c75e1620ba4b95f33a"
default['oradb11gR2']['oradb11gR2_oraInventory'] = "/home/oracle/oraInventory"
default['oradb11gR2']['oradb11gR2_home'] = "/opt/oracle/product/11.2/db"
default['oradb11gR2']['oradb11gR2_base'] = "/opt/oracle"
default['oradb11gR2']['dbName'] = "neo"
default['oradb11gR2']['port'] = "1521"
default['oradb11gR2']['syspwd'] = "passw0rd"

# Weblogic (fmw_* cookbooks)
default['fmw_wls']['remote_uri'] = "/weblogic/"
default['fmw_wls']['install_archive_name'] = "wls_121300.jar"
default['fmw_wls']['checksum'] = "6a5367abb241d0f07385c64587cfa54a79aff4ad7c4e87e2e43a74ef16f90ba2"
default['fmw_wls']['logDir'] = "/data/logs"
node.override['fmw']['middleware_home_dir'] = "/opt/oracle/middleware12c"
node.override['fmw']['weblogic_home_dir'] = "/opt/oracle/middleware12c/wlserver"
node.override['fmw']['version'] = "12.1.3"
node.override['fmw']['os_user'] = node['user']
node.override['fmw']['os_group'] = node['user_group']
node.override['fmw']['ora_inventory_dir'] = "/home/" + node['user'] + "/oraInventory"
node.override['fmw_wls']['source_file'] = node['host_download_folder'] + "/" + node['fmw_wls']['install_archive_name']
node.override['fmw_domain']['databag_key'] = "weblogic_domain"
node.override['fmw_domain']['domains_dir'] = "/opt/oracle/middleware12c/user_projects/domains"
#other Weblogic settings can be found at ./chefdevops/data_bags/fmw_domains/weblogic_domain.json

# Flash (flash cookbook)
default['flash']['remote_uri'] = "/linux/"
default['flash']['install_archive_name'] = "flash-plugin-11.2.202.632-release.x86_64.rpm"
default['flash']['checksum'] = "c025ccdec17cec266e3764e60a3304414373369e4c51c2dd5596b1a80997343c"

# Sonarqube (sonarqube cookbook)
default['sonar']['remote_uri'] = "/linux/"
default['sonar']['install_archive_name'] = "sonar-5.6-1.noarch.rpm"
default['sonar']['checksum'] = "14be11c765a32a8db9a5eb394083f5f0619c720b7be0910af686ac95356537c7"

# Endeca versions (used in all Endeca cookbooks)
default['endeca']['version'] = "11.2.0"
default['endeca']['mdex_version'] = "6.5.2"
default['endeca']['password'] = "Admin123"

# Endeca ToolsAndFrameworks (endeca_tf cookbook)
default['endeca_tf']['remote_uri'] = "/endeca/11.2/ToolsAndFrameworks/"
default['endeca_tf']['tf_install_file_name'] = "silent_install.sh"
default['endeca_tf']['tf_install_archive_name'] = "V78229-01-Oracle-Commerce-Experience-Manager-TAF-11.2-Linux.zip"
default['endeca_tf']['checksum'] = "7a7a45410c4705b572b2862449156a5e9cb286d17b91ba471677c5b3ac299a1a"
default['endeca_tf']['endeca_tf_home'] = "/opt/endeca/ToolsAndFrameworks"
default['endeca_tf']['endeca_tf_installUser'] = "oracle"
default['endeca_tf']['endeca_tf_installGroup'] = "dba"

# Endeca MDEX (endeca_mdex cookbook)
default['endeca_mdex']['remote_uri'] = "/endeca/11.2/MDEX/"
default['endeca_mdex']['mdex_install_file_name'] = "OCmdex6.5.2-Linux64_962107.bin"
default['endeca_mdex']['mdex_install_archive_name'] = "V78211-01-Oracle-Commerce-MDEX-6.5.2-Linux.zip"
default['endeca_mdex']['checksum'] = "b276fc2647d6fa96bf6f5594ee21e5b2e1b8cee9f952f1737a1e8dacfa723d7d"
default['endeca_mdex']['endeca_mdex_home'] = "/opt/endeca/MDEX"
default['endeca_mdex']['endeca_mdex_base'] = "/opt/endeca"

# Endeca MDEX (docker_endeca_mdex cookbook)
default['docker_endeca_mdex']['remote_uri'] = "/endeca/11.2/MDEX/"
default['docker_endeca_mdex']['mdex_install_file_name'] = "OCmdex6.5.2-Linux64_962107.bin"
default['docker_endeca_mdex']['mdex_install_archive_name'] = "V78211-01-Oracle-Commerce-MDEX-6.5.2-Linux.zip"
default['docker_endeca_mdex']['checksum'] = "b276fc2647d6fa96bf6f5594ee21e5b2e1b8cee9f952f1737a1e8dacfa723d7d"
default['docker_endeca_mdex']['endeca_mdex_home'] = "/opt/endeca/MDEX"
default['docker_endeca_mdex']['endeca_mdex_base'] = "/opt/endeca"

# Endeca Platform Services (endeca_ps cookbook)
default['endeca_ps']['remote_uri'] = "/endeca/11.2/PlatformServices/"
default['endeca_ps']['ps_install_file_name'] = "OCplatformservices11.2.0-Linux64.bin"
default['endeca_ps']['ps_install_archive_name'] = "V78226-01-Oracle-Commerce-Guided-Search-Platform-Services-11.2-Linux.zip"
default['endeca_ps']['checksum'] = "628486176a27acb5b0a0040b69f27fd5cb158a831469889d157be10184a690a2"
default['endeca_ps']['endeca_ps_home'] = "/opt/endeca/PlatformServices"

# Endeca CAS (endeca_cas cookbook)
default['endeca_cas']['remote_uri'] = "/endeca/11.2/CAS/"
default['endeca_cas']['cas_install_file_name'] = "OCcas11.2.0-Linux64.bin"
default['endeca_cas']['cas_install_archive_name'] = "V78204-01-Oracle-Commerce-CAS-11.2-Linux.zip"
default['endeca_cas']['checksum'] = "5db95a0c5152afbe2722880d58936065f757f7fe92330392e75e6f533bf84c79"
default['endeca_cas']['endeca_cas_home'] = "/opt/endeca/CAS"

# Gradle (gradle cookbook)
default['gradle']['remote_uri'] = "/linux/"
default['gradle']['install_archive_name'] = "gradle-2.2-all.zip"
default['gradle']['checksum'] = "65fc05f787c7344cc8834dc13a445c91ea3712a987f5958b8489422484d7371b"
default['gradle']['gradle_home'] = "/opt/gradle-2.2"

# Apache Ant (ant cookbook)
default['ant']['remote_uri'] = "/linux/"
default['ant']['install_archive_name'] = "apache-ant-1.7.1-bin.zip"
default['ant']['ant_home'] = "/opt/apache-ant-1.7.1"
default['ant']['checksum'] = "879a6ae22ae022a944a43f6dea21acfdec0acb30175e743664536f89626c0281"

# Eclipse (eclipse cookbook)
default['eclipse']['install_archive_name'] = "eclipse-jee-luna-R-linux-gtk-x86_64-plugins.tar.gz"
default['eclipse']['checksum'] = "fcf887cf8a047b81269ac506b84ca0aa807c7c1fbe4ca1fd240e7e2f758e4295"
default['eclipse']['remote_uri'] = "/eclipse/"
default['eclipse']['eclipse_home'] = "/opt/eclipse"
default['eclipse']['options'] = { '^-Xms.*m' => '-Xms1024m', '^-Xmx.*m' => '-Xmx2048m' }
default['eclipse']['installations'] = {}

# SQL Developer (sqldeveloper cookbook)
default['sqldeveloper']['remote_uri'] = "/oracle/"
default['sqldeveloper']['install_archive_name'] = "sqldeveloper-4.1.2.20.64-1.noarch.rpm"
default['sqldeveloper']['checksum'] = "f2b9fe3dccc90a3bdf94de18d757a7fe9cfbd46179f76cafdc5523f9953df131"
default['sqldeveloper']['sqldeveloper_bin'] = "/usr/local/bin/sqldeveloper"
default['sqldeveloper']['sqldeveloper_base'] = "/opt/sqldeveloper"

# OJDB6 driver
default['ojdbc']['install_archive_name'] = "ojdbc6_11204.jar"
default['ojdbc']['remote_uri'] = "/oracle/"
default['odjbc']['checksum'] = "e70213917b5f0d7448072836da07c709930b89dd4b0cc14a1eef814836747900"
